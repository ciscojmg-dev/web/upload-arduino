var Avrgirl = require('avrgirl-arduino');
const SerialPort = require('serialport');
const inquirer = require('inquirer');

var listofports = [];
SerialPort.list().then(function (ports) {
    let serialPortChoices = {};
    serialPortChoices.type = 'list';
    serialPortChoices.message = 'Choose serial port you would like to connect:'
    serialPortChoices.name = "serialport";

    let listOfdevices = [];
    ports.forEach(function (port) {
        listOfdevices.push(port.path);
    })


    inquirer.prompt([{
        type: 'list',
        name: 'port',
        message: 'Selecciona el puerto',
        choices: listOfdevices,
    },{
        type: 'list',
        name: 'code',
        message: 'Selecciona el programa',
        choices: ['main.hex', 'main2.hex'],
    }])

        .then(answers => {
            console.log('port: ' + answers.port);
            console.log('code: ' + answers.code);
            const board = 'mega';
            var avrgirl = new Avrgirl({
                board: board,
                port: answers.port,
                debug: true
            });

            avrgirl.flash(answers.code, function (error) {
                if (error) {
                    console.error(error);
                } else {
                    console.info('board: ' + board + ' done.');
                }
            });
        })

});